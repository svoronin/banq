package ua.pp.voronin.sergii.softserve.banq;

import graphql.schema.GraphQLSchema;

import io.leangen.graphql.GraphQLSchemaGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ua.pp.voronin.sergii.softserve.banq.service.CustomerService;

@SpringBootApplication
public class StartupService {

    private CustomerService customerService = new CustomerService();

    public static void main(String[] args) {
        SpringApplication.run(StartupService.class, args);
    }

    @Bean
    public GraphQLSchema initGraphQl() {
        return new GraphQLSchemaGenerator()
                .withOperationsFromSingleton(customerService)
                .generate();
    }

}