package ua.pp.voronin.sergii.softserve.banq.resource;

import com.fasterxml.jackson.annotation.JsonProperty;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.analysis.MaxQueryComplexityInstrumentation;
import graphql.schema.GraphQLSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountResource {

    private GraphQL graphQl;

    @Autowired
    public AccountResource(GraphQLSchema schema) {
        this.graphQl = GraphQL
                .newGraphQL(schema)
                .instrumentation(
                        new MaxQueryComplexityInstrumentation(2)
                )
                .build();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ExecutionResult accessGraphQl(GraphQlRequest request) {
        return graphQl.execute(ExecutionInput.newExecutionInput()
                .query(request.query)
                .operationName(request.operationName)
                .build());
    }
}

class GraphQlRequest {
    @JsonProperty String operationName;
    @JsonProperty String query;
}