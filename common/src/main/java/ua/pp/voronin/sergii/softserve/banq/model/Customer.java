package ua.pp.voronin.sergii.softserve.banq.model;

import java.util.List;

public class Customer {

    private Pii pii;
    private List<Account> accounts;

    public Customer(Pii pii, List<Account> accounts) {
        this.pii = pii;
        this.accounts = accounts;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public Pii getPii() {
        return pii;
    }

    public void setPii(Pii pii) {
        this.pii = pii;
    }
}