package ua.pp.voronin.sergii.softserve.banq.model;

import java.util.List;

public class Account {

    private String id;
    private List<Card> cards;

    public Account(String id, List<Card> cards) {
        this.id = id;
        this.cards = cards;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }
}