package ua.pp.voronin.sergii.softserve.banq.model;

public class Address {

    private String country;
    private String city;
    private String streetLine1;
    private String streetLine2;

    public Address(String country, String city, String streetLine1, String streetLine2) {
        this.country = country;
        this.city = city;
        this.streetLine1 = streetLine1;
        this.streetLine2 = streetLine2;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetLine1() {
        return streetLine1;
    }

    public void setStreetLine1(String streetLine1) {
        this.streetLine1 = streetLine1;
    }

    public String getStreetLine2() {
        return streetLine2;
    }

    public void setStreetLine2(String streetLine2) {
        this.streetLine2 = streetLine2;
    }
}