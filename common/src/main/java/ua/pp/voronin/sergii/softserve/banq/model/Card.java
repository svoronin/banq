package ua.pp.voronin.sergii.softserve.banq.model;

public class Card {

    private String id;
    private Pin pin;
    private String name;

    public Card(String id, Pin pin, String name) {
        this.id = id;
        this.pin = pin;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Pin getPin() {
        return pin;
    }

    public void setPin(Pin pin) {
        this.pin = pin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}