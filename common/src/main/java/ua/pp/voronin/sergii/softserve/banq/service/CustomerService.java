package ua.pp.voronin.sergii.softserve.banq.service;

import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLComplexity;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import ua.pp.voronin.sergii.softserve.banq.SampleDataGenerator;
import ua.pp.voronin.sergii.softserve.banq.model.Account;
import ua.pp.voronin.sergii.softserve.banq.model.Card;
import ua.pp.voronin.sergii.softserve.banq.model.Customer;
import ua.pp.voronin.sergii.softserve.banq.model.Pin;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CustomerService {

    private List<Customer> customers = SampleDataGenerator.generate();

    @GraphQLQuery(name = "customers")
    public List<Customer> findByName(@GraphQLArgument(name = "name") final String name) {
        if (name == null) {
            return customers;
        }
        return customers.stream().filter(
                customer -> name.equals(customer.getPii().getFirstName())
        ).collect(Collectors.toList());
    }

    @GraphQLMutation(name = "addCart")
    public Customer addCart(@GraphQLArgument(name = "account") String accountId,
                            @GraphQLArgument(name = "card") final String cardId,
                            @GraphQLArgument(name = "name") final String name) {
        return customers.stream().filter(cust -> {
            Optional<Account> match = cust.getAccounts().stream().filter(account -> accountId.equals(account.getId())).findFirst();
            match.ifPresent(account -> account.getCards().add(new Card(cardId, new Pin("0000"), name)));
            return match.isPresent();
        }).findAny().orElse(null);
    }

    public List<Customer> getAllCustomers() {
        return customers;
    }
}
