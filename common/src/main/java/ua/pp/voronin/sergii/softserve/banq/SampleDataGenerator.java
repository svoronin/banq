package ua.pp.voronin.sergii.softserve.banq;

import ua.pp.voronin.sergii.softserve.banq.model.Account;
import ua.pp.voronin.sergii.softserve.banq.model.Address;
import ua.pp.voronin.sergii.softserve.banq.model.Card;
import ua.pp.voronin.sergii.softserve.banq.model.Customer;
import ua.pp.voronin.sergii.softserve.banq.model.Pii;
import ua.pp.voronin.sergii.softserve.banq.model.Pin;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SampleDataGenerator {

    public static List<Customer> generate() {
        return Arrays.asList(
                new Customer(
                        new Pii("Amily","Amber",
                                new Address("France", "Paris", "Champ de Mars", "5 Avenue Anatole")),
                        list(new Account("1", list(
                                new Card("4321 1234 5678 91011", new Pin("1234"), "alpha"))))),
                new Customer(
                        new Pii("Billy","Ben",
                                new Address("Great Britain", "London", "Baker Str", "221b")),
                        list(new Account("2", Arrays.asList(
                                new Card("1234 1234 5678 91011", new Pin("4567"), "bravo"),
                                new Card("4567 1234 5678 91011", new Pin("0007"), "beretta"))))),
                new Customer(
                        new Pii("Chris","Chrysler",
                                new Address("Italy", "Rome", "Piazza del Colosseo", "1")),
                        list(new Account("3", list(
                                new Card("8910 1234 5678 91011", new Pin("0198"), "charlie"))))),
                new Customer(
                        new Pii("David","Duchovny",
                                new Address("USA", "San Francisco", "Twin Peaks", "100")),
                        list(new Account("4", list(
                                new Card("5000 1234 5678 91011", new Pin("1234"), "delta")))))
        );
    }

    private static <T> List<T> list(T ... t) {
        return Arrays.stream(t).collect(Collectors.toList());
    }
}
