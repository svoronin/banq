package ua.pp.voronin.sergii.softserve.banq.model;

public class Pii {

    private String firstName;
    private String lastName;
    private Address residence;

    public Pii(String firstName, String lastName, Address residence) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.residence = residence;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getResidence() {
        return residence;
    }

    public void setResidence(Address residence) {
        this.residence = residence;
    }
}