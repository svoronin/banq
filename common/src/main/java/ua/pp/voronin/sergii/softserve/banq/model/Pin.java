package ua.pp.voronin.sergii.softserve.banq.model;

public class Pin {

    private static final int EXPECTED_PIN_LENGTH = 4;
    private int hash;

    public Pin(String pin) {
        hash = hash(pin);
    }

    public void set(String pin) {
        hash = hash(pin);
    }

    public boolean isCorrect(String pin) {
        return hash == hash(pin);
    }

    private static int hash(String pin) {
        if (pin == null || pin.length() != EXPECTED_PIN_LENGTH) {
            throw new IllegalArgumentException(String.format("We require PIN to be exacttly %d digits long", EXPECTED_PIN_LENGTH));
        }
        if (!pin.chars().allMatch(Character::isDigit)) {
            throw new IllegalArgumentException("Provided PIN has invalid symbols. Only digits allowed.");
        }

        final int HASHING_FACTOR = 51;
        int hash = 0;
        for(char symbol : pin.toCharArray()) {
            hash += symbol * HASHING_FACTOR;
        }
        return hash;
    }
}
